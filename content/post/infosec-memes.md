+++
author = "xanhacks"
title = "Infosec Memes"
date = "2021-12-23"
description = "List of homemade infosec memes"
tags = [
    "memes"
]
categories = [
    "others"
]
slug = "infosec-memes"
image = "/img/infosec-memes/gdb-si.jpg"
+++

List of **homemade** infosec memes.


![travis.png](/img/infosec-memes/travis.png)

---

![steg.jpg](/img/infosec-memes/steg.jpg)

---

![steg-guessing.png](/img/infosec-memes/steg-guessing.png)

---

![sqlmap.jpg](/img/infosec-memes/sqlmap.jpg)

---

![rm-root.jpg](/img/infosec-memes/rm-root.jpg)

---

![powershell.jpg](/img/infosec-memes/powershell.jpg)

---

![nmap.jpg](/img/infosec-memes/nmap.jpg)

---

![lose-revshell.jpg](/img/infosec-memes/lose-revshell.jpg)

---

![image.png](/img/infosec-memes/image.png)

---

![hackthebox.jpg](/img/infosec-memes/hackthebox.jpg)

---

![git-folder.jpg](/img/infosec-memes/git-folder.jpg)

---

![gdb-si.jpg](/img/infosec-memes/gdb-si.jpg)

---

![fuzzing.jpg](/img/infosec-memes/fuzzing.jpg)

---

![format-string.jpg](/img/infosec-memes/format-string.jpg)

---

![exploit-no-session.jpg](/img/infosec-memes/exploit-no-session.jpg)

---

![rename-remove.jpg](/img/infosec-memes/rename-remove.jpg)

---

![bb-vuln-low-medium.jpg](/img/infosec-memes/bb-vuln-low-medium.jpg)

---

![five-dups-in-a-row.jpg](/img/infosec-memes/five-dups-in-a-row.jpg)

---

![huge-scope.jpg](/img/infosec-memes/huge-scope.jpg)

---

I paste the following command here, if I need to re-generate this Markdown article :

```bash
 ls -t static/img/infosec-memes | tac | awk '-F ' '{ print "\n![" $1 "](/img/infosec-memes/" $1 ")\n\n---" }'
```

Hope you enjoyed it !

Do not forget to bookmark this page, I often add new memes.
