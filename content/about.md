---
title: About
description: Description of my technical background
date: '2021-11-22'
links:
  - title: HeroCTF 
    description: An online cybersecurity competition (CTF) for beginner and advanced players. 
    website: https://heroctf.fr
    image: https://raw.githubusercontent.com/HeroCTF/heroctf.github.io/master/assets/images/logo.png 

  - title: xanhacks's wiki / docs
    description: An online documentation with cheatsheets about infosec / CTF. 
    website: https://docs.xanhacks.xyz
    image: https://raw.githubusercontent.com/HeroCTF/heroctf.github.io/master/assets/images/about/team/xanhacks.png 

  - title: Fast decoder
    description: A light web-based encoder / decoder (base64, URL encode, ...). 
    website: https://decoder.xanhacks.xyz
    image: https://decoder.xanhacks.xyz/favicon.ico 

  - title: Dorks builder
    description: Website that generates predefined google dorks on a domain. 
    website: https://dorks.xanhacks.xyz
    image: https://dorks.xanhacks.xyz/favicon.png
aliases:
  - about
menu:
    main: 
        weight: -90
        params:
            icon: user
---

## 📜 w'h'o$(echo am)i

Hello, I'm **xanhacks** 🥖, a cybersecurity student. Welcome on my blog !

## 🔖 Topics

- web pentesting & vulnerability research
- malware, reverse & hunting
- dev, python / web mostly
- sys, linux, docker, gitlab, devops automation tools
- everything as long as I can learn some new stuff... 

Online profiles : [Root-Me](https://www.root-me.org/xanhacks), [HackTheBox](https://app.hackthebox.com/profile/176254), [TryHackMe](https://tryhackme.com/p/xanhacks), [YesWeHack](https://yeswehack.com/hunters/xanhacks), [Huntr](https://www.huntr.dev/users/xanhacks/), [CryptoHack](https://cryptohack.org/user/xanhacks/), [Github](https://github.com/xanhacks/), [Gitlab](https://gitlab.com/xanhacks)
