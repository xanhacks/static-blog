# xanhacks' blog

**Live demo at :** https://www.xanhacks.xyz/

## Docs

Start a local server :

```bash
hugo server -b "http://localhost:1313/"
```

Create a new post :

```bash
hugo new --kind post posts/name-of-the-post.md
```

## Made with

- [gohugo](https://gohugo.io/)

new theme :

- [stack](https://github.com/CaiJimmy/hugo-theme-stack)

old theme :

- [congo](https://github.com/jpanther/congo)
